<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function () {
    // public
    Route::post('login', 'AuthController@login');
    // tambahkan sekalian untuk register dan logout :
    Route::post('register', 'AuthController@register');
    Route::middleware(['cors'])->group(function () {
        Route::get('categories/random/{count}', 'CategoryController@random');
        Route::get('books/top/{count}', 'BookController@top');
        Route::get('categories', 'CategoryController@index');
    });
    
    Route::get('books', 'BookController@index');
    Route::get('book/{id}', 'BookController@view')->where('id', '[0-9]+');
    
    // private
Route::middleware('auth:api')->group(function () {
    Route::post('logout', 'AuthController@logout');
});
    // Route::post('logout', 'AuthController@logout');
    
    // Route::get('/book', function () {
    //     return new BookResource(Book::find(1));
    // });
});